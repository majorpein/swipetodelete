//
//  ActionItem.swift
//  SwipeToDelete
//
//  Created by Aleksandr Anosov on 08.07.2020.
//  Copyright © 2020 Aleksandr Anosov. All rights reserved.
//

import UIKit

class ActionItem {
    typealias Action = (IndexPath) -> Void

    let view: UIView
    let action: Action

    init(viewModel: ActionViewModel, action: @escaping Action) {
        let view = ActionView(frame: CGRect(x: 0, y: 0, width: SwipeToAction.Constants.width, height: 0))
        view.setup(viewModel: viewModel)
        self.view = view
        self.action = action
    }
}

class DeleteActionItem: ActionItem {
    init(action: @escaping Action) {
        let viewModel = ActionViewModel(
            text: "Удалить",
            color: .red,
            textColor: .white
        )
        super.init(viewModel: viewModel, action: action)
    }
}
