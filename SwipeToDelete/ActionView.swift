//
//  ActionView.swift
//  SwipeToDelete
//
//  Created by Aleksandr Anosov on 08.07.2020.
//  Copyright © 2020 Aleksandr Anosov. All rights reserved.
//

import UIKit

struct ActionViewModel {
    let text: String
    let color: UIColor
    let textColor: UIColor
}

class ActionView: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var textLabel: UILabel!

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialSetup()
    }

    func initialSetup() {
        Bundle(for: ActionView.self).loadNibNamed("ActionView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }

    func setup(viewModel: ActionViewModel) {
        contentView.backgroundColor = viewModel.color
        textLabel.text = viewModel.text
        textLabel.textColor = viewModel.textColor
    }
}
