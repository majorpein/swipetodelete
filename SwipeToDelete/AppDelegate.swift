//
//  AppDelegate.swift
//  SwipeToDelete
//
//  Created by Aleksandr Anosov on 08.07.2020.
//  Copyright © 2020 Aleksandr Anosov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }



}

