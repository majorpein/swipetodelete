//
//  SwipeToActionRecognizer.swift
//  SwipeToDelete
//
//  Created by Aleksandr Anosov on 08.07.2020.
//  Copyright © 2020 Aleksandr Anosov. All rights reserved.
//

import UIKit

protocol SwipeToActionDelegate: AnyObject {
    func actions(for indexPath: IndexPath) -> [ActionItem]
}

class SwipeToAction: NSObject {
    enum Constants {
        static let duration: TimeInterval = 0.3
        static let shortDuration: TimeInterval = 0.1
        static let width: CGFloat = 100
    }

    enum ActionsState {
        case none
        case all(IndexPath)
        case single(IndexPath)
    }

    weak var delegate: SwipeToActionDelegate?

    private let panRecognizer: UIPanGestureRecognizer
    private let tapRecognizer: UITapGestureRecognizer
    private weak var collectionView: UICollectionView?

    private var cell: UICollectionViewCell?
    private var actions: [ActionItem] = []
    private var initialOffset: CGFloat = 0
    private var state: ActionsState = .none

    override init() {
        panRecognizer = UIPanGestureRecognizer()
        tapRecognizer = UITapGestureRecognizer()
        super.init()

        panRecognizer.addTarget(self, action: #selector(onPan(_:)))
        panRecognizer.maximumNumberOfTouches = 1
        panRecognizer.delegate = self

        tapRecognizer.addTarget(self, action: #selector(onTap(_:)))
        tapRecognizer.delegate = self
    }

    func add(on collectionView: UICollectionView) {
        self.collectionView = collectionView
        collectionView.addGestureRecognizer(panRecognizer)
        collectionView.addGestureRecognizer(tapRecognizer)
    }

    private func resetState() {
        cell = nil
        actions = []
        initialOffset = 0
        state = .none
    }
}

//MARK: - Animations

extension SwipeToAction {
    private func setIndexedOffset(_ offset: CGFloat, for action: ActionItem, at index: Int, cellWidth: CGFloat) {
        let actionOffset = offset * CGFloat(index + 1) / CGFloat(actions.count)
        action.view.frame.origin.x = cellWidth + actionOffset
        action.view.frame.size.width = Constants.width
    }

    private func setSingleActionOffset(_ offset: CGFloat, for action: ActionItem, at index: Int, cellWidth: CGFloat) {
        action.view.frame.origin.x = cellWidth + offset
        action.view.frame.size.width = -offset
    }

    private func openFirstActionOnly(offset: CGFloat, on cell: UICollectionViewCell) {
        actions.enumerated().forEach { index, action in
            switch state {
            case .all(let indexPath):
                if index == 0 {
                    UIView.animate(
                        withDuration: Constants.shortDuration,
                        animations: {
                            self.setSingleActionOffset(offset, for: action, at: index, cellWidth: cell.bounds.width)
                        },
                        completion: { _ in
                            self.state = .single(indexPath)
                        }
                    )
                } else {
                    setIndexedOffset(offset, for: action, at: index, cellWidth: cell.bounds.width)
                }
            case .single, .none:
                if index == 0 {
                    setSingleActionOffset(offset, for: action, at: index, cellWidth: cell.bounds.width)
                } else {
                    setIndexedOffset(offset, for: action, at: index, cellWidth: cell.bounds.width)
                }
            }
        }
    }

    private func hideFirstActionOnly(offset: CGFloat, on cell: UICollectionViewCell) {
        switch state {
        case .single(let indexPath):
            actions.enumerated().forEach { index, action in
                if index == 0 {
                    UIView.animate(
                        withDuration: Constants.shortDuration,
                        animations: {
                            self.setIndexedOffset(offset, for: action, at: index, cellWidth: cell.bounds.width)
                        },
                        completion: { _ in
                            self.state = .all(indexPath)
                        }
                    )
                } else {
                    setIndexedOffset(offset, for: action, at: index, cellWidth: cell.bounds.width)
                }
            }
        case .all, .none:
            actions.enumerated().forEach { index, action in
                setIndexedOffset(offset, for: action, at: index, cellWidth: cell.bounds.width)
            }
        }
    }

    private func openActions(_ actions: [ActionItem], on cell: UICollectionViewCell) {
        let offset = -CGFloat(actions.count) * Constants.width

        UIView.animate(withDuration: Constants.duration) {
            cell.contentView.frame.origin.x = offset
            actions.enumerated().forEach { index, action in
                self.setIndexedOffset(offset, for: action, at: index, cellWidth: cell.bounds.width)
            }
        }
    }

    private func hideActions(_ actions: [ActionItem],
                             on cell: UICollectionViewCell,
                             shouldRemoveFromSuperview: Bool = false) {
        UIView.animate(
            withDuration: Constants.duration,
            animations: {
                cell.contentView.frame.origin.x = 0
                actions.forEach {
                    $0.view.frame.origin.x = cell.bounds.width
                }
            },
            completion: { _ in
                if shouldRemoveFromSuperview {
                    actions.forEach {
                        $0.view.removeFromSuperview()
                    }
                }
            }
        )
    }
}

//MARK: - UIGestureRecognizerDelegate

extension SwipeToAction: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == panRecognizer {
            if abs((panRecognizer.velocity(in: collectionView)).x) > abs((panRecognizer.velocity(in: collectionView)).y) {
                if panRecognizer.velocity(in: collectionView).x > 0 {
                    if case .all(let indexPathOfActions) = state,
                        let indexPath = collectionView?.indexPathForItem(at: panRecognizer.location(in: collectionView)),
                        indexPath == indexPathOfActions {
                            return true
                    } else if let cell = cell {
                        hideActions(actions, on: cell, shouldRemoveFromSuperview: true)
                    }
                    resetState()
                    return false
                }
                return true
            } else if let cell = cell {
                hideActions(actions, on: cell, shouldRemoveFromSuperview: true)
            }
            resetState()
            return false
        } else if gestureRecognizer == tapRecognizer {
            let point = gestureRecognizer.location(in: collectionView)

            guard let indexPath = collectionView?.indexPathForItem(at: point),
                let cell = collectionView?.cellForItem(at: indexPath),
                cell == self.cell,
                let cellPoint = collectionView?.convert(point, to: cell) else {
                    if let cell = self.cell {
                        hideActions(actions, on: cell, shouldRemoveFromSuperview: true)
                    }
                    resetState()
                    return false
            }

            if actions.map({ $0.view.frame }).first(where: { $0.contains(cellPoint)}) == nil {
                if let cell = self.cell {
                    hideActions(actions, on: cell, shouldRemoveFromSuperview: true)
                }
                resetState()
                return false
            } else {
                return true
            }
        }

        return false
    }
}

//MARK: - Tap Recognizer

extension SwipeToAction {
    @objc
    private func onTap(_ recognizer: UIPanGestureRecognizer) {
        let point = recognizer.location(in: collectionView)

        guard let indexPath = collectionView?.indexPathForItem(at: point),
            let cell = collectionView?.cellForItem(at: indexPath),
            cell == self.cell,
            let cellPoint = collectionView?.convert(point, to: cell) else {
                return
        }

        actions.forEach {
            if $0.view.frame.contains(cellPoint) {
                $0.action(indexPath)
                return
            }
        }
    }
}

//MARK: - Pan Recognizer

extension SwipeToAction {
    @objc
    private func onPan(_ recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
            began(on: recognizer.location(in: collectionView))
        case .changed:
            changed(to: recognizer.translation(in: collectionView))
        case .ended:
            ended(velocity: recognizer.velocity(in: collectionView))
        default:
            break
        }
    }

    private func began(on point: CGPoint) {
        guard let indexPath = collectionView?.indexPathForItem(at: point),
            let cell = collectionView?.cellForItem(at: indexPath) else {
                if let cell = self.cell {
                    hideActions(actions, on: cell, shouldRemoveFromSuperview: true)
                }
                resetState()
                return
        }

        state = .all(indexPath)

        if let previousCell = self.cell, cell != previousCell {
            hideActions(actions, on: previousCell, shouldRemoveFromSuperview: true)
        }

        if cell == self.cell {
            initialOffset = cell.contentView.frame.origin.x
        } else {
            self.cell = cell
            self.actions = delegate?.actions(for: indexPath) ?? []

            actions.forEach {
                $0.view.frame = CGRect(x: cell.bounds.width, y: 0, width: $0.view.bounds.width, height: cell.bounds.height)
                cell.insertSubview($0.view, at: 0)
            }
        }
    }

    private func changed(to point: CGPoint) {
        guard let cell = cell else {
            return
        }

        let offset = initialOffset + point.x
        cell.contentView.frame.origin.x = offset

        if offset < -CGFloat(actions.count) * Constants.width {
            openFirstActionOnly(offset: offset, on: cell)
        } else {
            hideFirstActionOnly(offset: offset, on: cell)
        }
    }

    private func ended(velocity: CGPoint) {
        guard let cell = cell else {
            return
        }

        if velocity.x <= 0 {
            switch state {
            case .all, .none:
                openActions(actions, on: cell)
            case .single:
                guard let indexPath = collectionView?.indexPath(for: cell) else {
                    assertionFailure("Couldn't get indexPath of a cell")
                    return
                }
                actions.first?.action(indexPath)
                hideActions(actions, on: cell)
                resetState()
            }
        } else {
            hideActions(actions, on: cell)
            resetState()
        }
    }
}
