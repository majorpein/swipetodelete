//
//  ViewController.swift
//  SwipeToDelete
//
//  Created by Aleksandr Anosov on 08.07.2020.
//  Copyright © 2020 Aleksandr Anosov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!

    let swipeToAction = SwipeToAction()

    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.delegate = self
        collectionView.dataSource = self

        setupSwipeToAction()
    }

    func setupSwipeToAction() {
        swipeToAction.delegate = self
        swipeToAction.add(on: collectionView)
    }
}

extension ViewController: SwipeToActionDelegate {
    func actions(for indexPath: IndexPath) -> [ActionItem] {
        let deleteAction = DeleteActionItem() { indexPath in
            print("delete item at \(indexPath.item)")
        }
        let customActionViewModel = ActionViewModel(
            text: "Действие",
            color: .yellow,
            textColor: .black
        )
        let customAction = ActionItem(viewModel: customActionViewModel) { indexPath in
            print("customAction item at \(indexPath.item)")
        }
        return [deleteAction, customAction]
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: 100)
    }
}

extension ViewController: UICollectionViewDelegate {

}

extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier: "BlueCell", for: indexPath)
    }


}
